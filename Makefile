PREFIX ?= /usr

all:

install:
	mkdir -p /usr/lib/gnome-panel
	cp topshelf.py     /usr/lib/gnome-panel/

	cp topshelf.server /usr/lib/bonobo/servers/

	cp topshelf-24.png      $(PREFIX)/share/pixmaps
	cp topshelf-24-off.png  $(PREFIX)/share/pixmaps
	cp topshelf-48.png      $(PREFIX)/share/pixmaps
	cp topshelf-svg.svg     $(PREFIX)/share/pixmaps
	cp topshelf-svg-off.svg $(PREFIX)/share/pixmaps

	mkdir -p /usr/share/doc/topshelf
	cp topshelf.docbook /usr/share/doc/topshelf

remove:
	rm /usr/lib/gnome-panel/topshelf.py

	rm /usr/lib/bonobo/servers/topshelf.server

	rm $(PREFIX)/share/pixmaps/topshelf-24.png
	rm $(PREFIX)/share/pixmaps/topshelf-24-off.png
	rm $(PREFIX)/share/pixmaps/topshelf-48.png
	rm $(PREFIX)/share/pixmaps/topshelf-svg.svg
	rm $(PREFIX)/share/pixmaps/topshelf-svg-off.svg

	rm /usr/share/doc/topshelf/topshelf.docbook
	rmdir /usr/share/doc/topshelf

