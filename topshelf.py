#!/usr/bin/env python
#------------------------------------------------------------------------------------
#
# TopShelf - A 'Currently Important Files' Applet
#
# Copyright (C) 2007  Alon Zakai ('Kripken')
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#-------------------------------------------------------------------------------------

import sys
import pickle
import os.path
import subprocess
import mimetypes
import urllib

import gobject
import gtk, gtk.gdk
import pygtk
pygtk.require('2.0')

import gnomeapplet

## Global constants

OPEN_APP = "gnome-open"
HELP_APP = "yelp"

HELP_FILE = "/usr/share/doc/topshelf/topshelf.docbook"

## Global utilities

def update_background(widget, backgroundtype, color, pixmap):
	# Needed for every widget on the panel
	widget.set_style(None)
	rc_style = gtk.RcStyle()
	widget.modify_style(rc_style)
	widget.modify_bg(gtk.STATE_NORMAL, color)

	if backgroundtype == gnomeapplet.PIXMAP_BACKGROUND:
		style = widget.get_style().copy()
		style.bg_pixmap[gtk.STATE_NORMAL] = pixmap
		widget.set_style(style);
	elif backgroundtype == gnomeapplet.COLOR_BACKGROUND:
		widget.modify_bg(gtk.STATE_NORMAL, color)

def popup_warning(window, message):
	dlg = gtk.MessageDialog(parent         = window,
	                        flags          = gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
	                        buttons        = gtk.BUTTONS_OK,
	                        message_format = message,
	                        type           = gtk.MESSAGE_WARNING)

	window.present() # Ensure we are on top, for our losing keep_above to not hide us
	if window is not None:
		window.set_keep_above(False)
	result = dlg.run()
	dlg.destroy()
	if window is not None:
		window.set_keep_above(True)

def popup_question(window, message):
	dlg = gtk.MessageDialog(parent         = window,
	                        flags          = gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
	                        buttons        = gtk.BUTTONS_YES_NO,
	                        message_format = message,
	                        type           = gtk.MESSAGE_QUESTION)

	window.present() # Ensure we are on top, for our losing keep_above to not hide us
	if window is not None:
		window.set_keep_above(False)
	result = dlg.run()
	dlg.destroy()
	if window is not None:
		window.set_keep_above(True)

	if result == gtk.RESPONSE_YES:
		return True
	elif result == gtk.RESPONSE_NO:
		return False
	elif result == gtk.RESPONSE_DELETE_EVENT:
		return False
	else:
		return False

def try_load_icon(icon_theme, icon_name, icon_size):
	try:
		return icon_theme.load_icon(icon_name, icon_size, 0)
	except gobject.GError:
		return None

def guess_mime_icon(filename):
	ICON_SIZE = 24

	icon_theme = gtk.icon_theme_get_default()

	if os.path.isdir(to_filename(filename)):
		ret = try_load_icon(icon_theme, "gnome-fs-directory", ICON_SIZE)
		if ret is None:
			ret = try_load_icon(icon_theme, "gtk-directory", ICON_SIZE)
		return ret

	filename = mimetypes.guess_type(os.path.basename(filename))[0]

	if filename is None:
		return try_load_icon(icon_theme, "unknown", ICON_SIZE)

	ret = try_load_icon(icon_theme, filename, ICON_SIZE)

	filename = filename.replace("/", "-")

	if ret is None:
		ret = try_load_icon(icon_theme, filename, ICON_SIZE)

	if ret is None:
		ret = try_load_icon(icon_theme, "gnome-mime-" + filename, ICON_SIZE)

	if ret is None:
		ret = try_load_icon(icon_theme, "gnome-mime-" + filename[0:filename.index("-")], ICON_SIZE)

	if ret is None:
		ret = try_load_icon(icon_theme, "unknown", ICON_SIZE)

	return ret

def to_filename(uri_or_something):
	return uri_or_something.replace("file://", "")


### Persistent data

PERSISTENT_FILE = os.path.join(os.path.expanduser("~"), ".topshelf.conf")

# Tests whether persistent data is 0.1, and hence needs migration
def test_point1(data):
	try:
		temp = data.data
		return False
	except AttributeError:
		return True
	
# Migrates persistent data from 0.1 to 0.2
def migrate_point1_to_point2(old):
	new = ModernPersistentData()
	new.set(current_files, old.current_files)
	new.set('w',           old.w)
	new.set('h',           old.h)
	return new

class PersistentData: ## Legacy!
	def __init__(self):
		self.current_files = []
		self.w = -1
		self.h = -1

current_files = "current_files" # A constant, really...
add_folder    = "add_folder"

DEFAULT_VALUES = { current_files : [],
                   add_folder    : os.path.expanduser("~"),
                   'x' : 400,   # For standalone app
                   'y' : 300,   # For standalone app
                   'w' : 600,
                   'h' : 350  }

class ModernPersistentData:
	def __init__(self):
		self.data = { }

	def get(self, key):
		try:
			return self.data[key]
		except KeyError:
			try:
				self.data[key] = DEFAULT_VALUES[key]
				return self.data[key]
			except KeyError:
				print "Fatal error: Key with no default value in ModernPersistentData"

	def set(self, key, value):
		self.data[key] = value


### Data for a single file

NORMAL_FG_COLOR  = "black"
DISABLE_FG_COLOR = "#FF0000"

class FileData:
	def __init__(self, filename, position, short_filename=None, mime_icon=None, color=None, strikethrough=None):
		self.filename  = filename
		self.position  = position

	def listify(self):
		return (self.filename, self.position, os.path.basename(self.filename), guess_mime_icon(self.filename), NORMAL_FG_COLOR, False)


### Main class

# full-path, position, short-filename, mime-type-icon, color
FILE_STORE_FULLPATH      = 0
FILE_STORE_POSITION      = 1
FILE_STORE_SHORTNAME     = 2
FILE_STORE_MIME_ICON     = 3
FILE_STORE_COLOR         = 4
FILE_STORE_STRIKETHROUGH = 5

class TopShelf:
	def __init__(self, applet, standalone_window=None):
		self.applet = applet
		self.applet.ts = self # Needed for standalone

#		self.applet.set_flags(gnomeapplet.EXPAND_MAJOR) # TODO: Needed?

		self.standalone_window = standalone_window

		self.window = None

		self.pressed  = False

		try:
			self.applet.set_tooltip_text("TopShelf")
		except AttributeError:
			pass # No tooltips, not the end of the world. Only python-gtk2 2.12 has them, i.e., Gutsy and above

		self.applet.connect("button_press_event", self.on_button_press)
		self.applet.drag_dest_set(gtk.DEST_DEFAULT_ALL, [('text/uri-list', 0, 81)], gtk.gdk.ACTION_LINK)
		self.applet.connect("drag_data_received", self.on_drag_data_received)
		self.applet.connect("key_press_event",    self.on_key_press)
		self.applet.connect("change_background",  self.on_change_background)
		self.applet.connect("change-size",        self.on_change_size)

		self.panel_image = gtk.Image()
		self.applet.add(self.panel_image)
		self.applet.show_all()

		# full-path, position, short-filename, mime-type-icon, color, strikethrough
		self.files_store = gtk.ListStore(str, int, str, gtk.gdk.Pixbuf, str, bool)

		try:
			self.unpickle()
		except IOError:
			self.persistent_data = ModernPersistentData()

		if self.standalone_window is not None:
			self.standalone_window.move(self.persistent_data.get('x'), self.persistent_data.get('y'))
			self.standalone_window.connect("configure-event", self.standalone_window_configure_event)
			self.standalone_window.set_keep_above(True)

		# Initial size
		self.size = self.applet.get_size()
		self.on_change_size(self.applet, self.size)


	# Init and setup

	def get_pixbufs(self, size):
		self.panel_pbs = []
		for pb_type in ["", "-off"]: # On, then off
			# With help from the Deskbar applet. Thanks!
			image_name = "/usr/share/pixmaps/topshelf-"
			if size <= 32:
				image_name += "24" + pb_type + ".png"
				size = 24
			else:
				image_name += "svg" + pb_type + ".svg"
				size -= 6

			self.panel_pbs.append(gtk.gdk.pixbuf_new_from_file_at_size(image_name, size, size))

	def set_image(self):
		if not self.pressed:
			self.panel_image.set_from_pixbuf(self.panel_pbs[0])
		else:		
			self.panel_image.set_from_pixbuf(self.panel_pbs[1])


	# Events

	def on_change_background(self, applet, backgroundtype, color, pixmap):
		update_background(applet, backgroundtype, color, pixmap)

	def on_change_size(self, applet, size):
		self.get_pixbufs(size)
		self.applet.set_size_request(size, size)
		self.set_image()

		self.size = size


	# Events

	def on_button_toggle(self, param1=None):
		if not self.pressed:
			self.show_window()
		else:
			self.hide_window()

	def on_button_press(self, btn, event):
		self.on_change_size(self.applet, self.applet.get_size())
		if event.type == gtk.gdk.BUTTON_PRESS and event.button == 1:
			self.on_button_toggle()
		elif event.type == gtk.gdk.BUTTON_PRESS and event.button == 3:
			propxml="""
				<popup name="button3">
				<menuitem name="Item 3" verb="Help"  label="_Help" pixtype="stock" pixname="gtk-help"/>
				<menuitem name="Item 4" verb="About" label="_About" pixtype="stock" pixname="gtk-about"/>
				</popup>"""
			verbs = [("About", self.on_about),
			         ("Help",  self.on_help)  ]
			self.applet.setup_menu(propxml, verbs, None)

	def on_files_button_press(self, btn, event):
		if event.button == 3:
			data = self.treeview.get_path_at_pos(int(event.x), int(event.y))
			if data is None:
				return True

			path = data[0]
			self.treeview.grab_focus()
			self.treeview.set_cursor(path)
			model, iters = self.get_selected()
			if len(iters) > 0:
				self.popup_menu.popup(None, None, None, event.button, event.time)
				return True

		return False

	def standalone_window_configure_event(self, param1=None, param2=None):
		x, y = self.standalone_window.get_position()
		if x != self.persistent_data.get('x') or y != self.persistent_data.get('y'):
			self.persistent_data.set('x', x)
			self.persistent_data.set('y', y)
			self.pickle() # This might thrash, if we get many many events at once. TODO.

	def window_state_event(self, widget, event):
		if event.changed_mask & gtk.gdk.WINDOW_STATE_ICONIFIED:
			self.hide_window()
			self.window.deiconify()

	def configure_event(self, widget, event):
		w, h = self.window.get_size()

		if w != self.persistent_data.get('w') or h != self.persistent_data.get('h'):
			self.persistent_data.set('w', w)
			self.persistent_data.set('h', h)

			self.pickle()

	def on_cursor_change(self, param1):
		model, iters = self.get_selected()
		for iter in iters:
			self.check_file(iter)

	# Actions

	def on_about(self, param1=None, param2=None):
		if self.window is not None:
			self.window.set_keep_above(False) # So we are not concealed!

		logo_pb = gtk.gdk.pixbuf_new_from_file_at_size("/usr/share/pixmaps/topshelf-svg.svg", 64, 64)

		dlg = gtk.AboutDialog()
		dlg.set_icon(logo_pb)

		dlg.set_name("TopShelf")
		dlg.set_program_name("TopShelf")
		dlg.set_version("0.3")
		dlg.set_comments("Currently Important Files Applet")
		dlg.set_authors(["Alon Zakai (Kripken)"])
		dlg.set_license("TopShelf is free software; you can redistribute it and/or\nmodify it under the terms of the GNU General Public License\nas published by the Free Software Foundation; either version\n2 of the License, or (at your option) any later version.")
		dlg.set_copyright("(c) 2007 Alon Zakai ('Kripken')")
		dlg.set_website("http://launchpad.net/topshelf")
		dlg.set_logo(logo_pb)

		dlg.run()
		dlg.destroy()

		if self.window is not None:
			self.window.set_keep_above(True)

		return False

	def on_open(self, param1=None, param2=None, param3=None):
		model, iters = self.get_selected()
		# TODO: open them all in one command (good for xmms etc.). Sadly gnome-open doesn't allow multiple files...
		for iter in iters:
			self.check_file(iter) # Update color depending on existence of file. Not needed if a thread does this!

			filename = model.get_value(iter, 0)
			ret = subprocess.call(OPEN_APP + " '" + filename + "'", shell=True)
			self.hide_window() # After opening, we hide... for convenience
			if ret != 0:
				popup_warning(self.window, "The file '" + filename + "' could not be opened due to an error.")

		return False

	def on_open_folder(self, param1=None, param2=None, param3=None):
		model, iters = self.get_selected()
		for iter in iters:
			self.check_file(iter) # Update color depending on existence of file. Not needed if a thread does this!

			dirname = os.path.dirname(model.get_value(iter, 0))
			ret = subprocess.call(OPEN_APP + " '" + dirname + "'", shell=True)
			self.hide_window() # After opening, we hide... for convenience
			if ret != 0:
				popup_warning(self.window, "The directory '" + dirname + "' could not be opened due to an error.")

		return False

	def on_move_up(self, param1=None):
		model, iters = self.get_selected()
		for iter in iters:
			old_val = model.get(iter, 1)[0]
			new_val = old_val - 1
			self.swap_file_position(model, iter, old_val, new_val)

		return False

	def on_move_down(self, param1=None):
		model, iters = self.get_selected()
		for iter in iters:
			old_val = model.get(iter, 1)[0]
			new_val = old_val + 1
			self.swap_file_position(model, iter, old_val, new_val)

		return False

	def on_add(self, wgt):
		self.window.set_keep_above(False) # So we are not concealed
		chooser = gtk.FileChooserDialog("Select a file to add to TopShelf:",
		                                self.window,
		                                gtk.FILE_CHOOSER_ACTION_OPEN,
		                                buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))

		chooser.set_select_multiple(True)
		chooser.set_current_folder(self.persistent_data.get(add_folder))

		response = chooser.run()
		if response == gtk.RESPONSE_OK:
			self.persistent_data.set(add_folder, chooser.get_current_folder())
			filenames = chooser.get_filenames()
		else:
			filenames = None

		chooser.destroy()

		self.window.set_keep_above(True)

		if filenames is not None:
			for filename in filenames:
				self.add_file(filename)

		return False

	def on_remove(self, wgt=None):
		model, iters = self.get_selected()
		if len(iters) > 0:
			if popup_question(self.window,
			                  "Are you sure you want to remove the selected files from the list? (The actual files will not be touched)"):
				filenames = []
				for iter in iters:
					filenames.append(model.get(iter, 0)[0])
				for filename in filenames:
					iter = self.get_file_iter_by_filename(filename)
					old_position = model.get(iter, 1)[0]
					self.files_store.remove(iter)
					self.remove_file_position(model, old_position)

				self.pickle()

		return False

	def on_drag_data_received(self, widget, drag_context, x, y, selection_data, info, timestamp):
		uris = selection_data.data.strip().split()
		for uri in uris:
			filename = urllib.url2pathname(uri)
			self.add_file(filename)

	def on_package(self, param1=None):
		if self.get_n_files() == 0:
			return

		chooser = gtk.FileChooserDialog("Select a filename to save the zipped files as:",
		                                self.window,
		                                gtk.FILE_CHOOSER_ACTION_SAVE,
		                                buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_SAVE,gtk.RESPONSE_OK))

		response = chooser.run()
		if response == gtk.RESPONSE_OK:
			filename = chooser.get_filenames()[0]
		else:
			response == None
		chooser.destroy()

		if package_name is None:
			return

		file_names = ""
		iter = self.files_store.get_iter_first()
		while iter is not None:
			file_names = file_names + " '" + urllib.url2pathname(self.files_store.get(iter, 0)[0]) + "' "
			iter = self.files_store.iter_next(iter)

		file_names = to_filename(file_names)

		command = "zip '" + package_name + "' " + file_names
		subprocess.call(command, shell=True)

		return False

	def on_help(self, param1=None, param2=None, param3=None):
		self.hide_window()
		ret = subprocess.call(HELP_APP + " '" + HELP_FILE + "'", shell=True)
		if ret != 0:
			popup_warning(self.window, "The help file could not be opened due to an error.")

		return False

	def on_key_press(self, widget, event):
		if event.keyval == 65307:
			self.hide_window()
		elif event.keyval == 65535:
			self.on_remove()
		elif event.keyval == 65474:
			self.check_all_files()


	# Persistency

	def unpickle(self):
		pickle_file = open(PERSISTENT_FILE, 'rb')
		self.persistent_data = pickle.load(pickle_file)
		pickle_file.close()

		# Migrate, if needed
		if test_point1(self.persistent_data):
			self.persistent_data = migrate_point1_to_point2(self.persistent_data)

		for file_data in self.persistent_data.get(current_files):
			self.files_store.insert(0, file_data.listify())

	def pickle(self):
		self.persistent_data.set(current_files, [])

		iter = self.files_store.get_iter_first()
		while iter is not None:
			self.persistent_data.get(current_files).append(FileData(*self.files_store.get(iter, *range(self.files_store.get_n_columns()))))
			iter = self.files_store.iter_next(iter)

		pickle_file = open(PERSISTENT_FILE, 'wb')
		pickle.dump(self.persistent_data, pickle_file)
		pickle_file.close()

	# File management

	def add_file(self, filename):
		self.files_store.insert(0, FileData(filename, self.get_n_files()).listify())

		self.pickle()

	def get_file_iter_by_filename(self, filename):
		class Ret:
			def __init__(self):
				self.val = None

			def search(self, m, p, i, filename):
				if m.get(i, 0)[0] == filename:
					self.val = i

		ret = Ret()
		self.files_store.foreach(ret.search, (filename))
		return ret.val

	# Window functions

	def show_window(self):
		if self.window is None:
			self.create_window()

		self.pressed = True

		# These four commands - present, keep-above, present, position - are very odd. But they work, in THIS order.
		self.window.present()
		self.window.set_keep_above(True) # Force it!
		self.window.present()
		self.position_window() # Needed here because of odd behavior otherwise (after adding, hide, show, it moved)

		self.check_all_files() # One manual operation, then later as a background thread
#		gobject.timeout_add(2000, self.check_files_thread) No thread for now, since it wastes CPU. Later, add an option to do this.

		self.set_image()

	def hide_window(self, param1=False, param2=False):
		if self.window is None:
			return True

		self.window.hide()

		while gtk.events_pending():
			gtk.main_iteration() # Ensure the hide actually happens before we show e.g. Help windows

		self.pressed = False

		self.set_image()

		return True # Stop delete events

	def create_window(self):
		self.window = gtk.Window()

		self.window.set_title("TopShelf")
		self.window.connect("delete-event",       self.hide_window)
		self.window.connect("window-state-event", self.window_state_event)
		self.window.connect("configure-event",    self.configure_event)
		self.window.connect("key-press-event",    self.on_key_press)

		self.window.set_keep_above(True)
		self.window.resize(self.persistent_data.get('w'), self.persistent_data.get('h'))

		self.window.drag_dest_set(gtk.DEST_DEFAULT_ALL, [('text/uri-list', 0, 81)], gtk.gdk.ACTION_LINK)
		self.window.connect("drag-data-received", self.on_drag_data_received)

		# Main view

		self.treeview = gtk.TreeView()
		self.treeview.set_model(self.files_store)
		self.treeview.get_selection().set_mode(gtk.SELECTION_MULTIPLE)

		col = self.append_column(self.treeview, gtk.TreeViewColumn("#",    gtk.CellRendererText(),   text  =1), 1)
		tmp = self.append_column(self.treeview, gtk.TreeViewColumn("",     gtk.CellRendererPixbuf(), pixbuf=3))
		tmp = self.append_column(self.treeview, gtk.TreeViewColumn("File", gtk.CellRendererText(),   text  =2), 2,
		                         foreground_id    = FILE_STORE_COLOR,
		                         strikethrough_id = FILE_STORE_STRIKETHROUGH)

		col.clicked() # Set default sort

		self.treeview.connect("row-activated" ,     self.on_open)
		self.treeview.connect("cursor-changed",     self.on_cursor_change)
		self.treeview.connect("button-press-event", self.on_files_button_press)

		scrolled_window = gtk.ScrolledWindow()
		scrolled_window.add(self.treeview)
		scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		# Toolbar & buttons on bottom

		toolbar = gtk.Toolbar()

		self.add_stock_button    (toolbar, gtk.STOCK_OPEN,                            self.on_open       , "Open file")
		self.add_altstock_button (toolbar, gtk.STOCK_DIRECTORY,      "Open Folder",   self.on_open_folder, "Open folder containing file")
		toolbar.add(gtk.SeparatorToolItem())
		self.add_stock_button    (toolbar, gtk.STOCK_GO_UP,                           self.on_move_up    , "Move file up in order")
		self.add_stock_button    (toolbar, gtk.STOCK_GO_DOWN,                         self.on_move_down  , "Move file down in order")
		toolbar.add(gtk.SeparatorToolItem())
		self.add_stock_button    (toolbar, gtk.STOCK_ADD,                             self.on_add        , "Add a file to the list")
		self.add_stock_button    (toolbar, gtk.STOCK_REMOVE,                          self.on_remove     , "Remove file from the list")
		toolbar.add(gtk.SeparatorToolItem())
		self.add_stock_button    (toolbar, gtk.STOCK_HELP,                            self.on_help       , "Show help")
		self.add_stock_button    (toolbar, gtk.STOCK_ABOUT,                           self.on_about      , "About TopShelf")
#		self.add_pixbuf_button   (toolbar, guess_mime_icon("a.zip"), "Package Files", self.on_package)

		# Main vbox

		vbox = gtk.VBox()
		vbox.add(scrolled_window)
		sep = gtk.HSeparator()
		vbox.add(sep)
		vbox.child_set_property(sep, "expand", False)
		vbox.add(toolbar)
		vbox.child_set_property(toolbar, "expand", False)

		# Popup menu

		self.popup_menu = gtk.Menu()
		self.add_menuitem   (self.popup_menu, gtk.STOCK_OPEN,                     self.on_open)
		self.add_altmenuitem(self.popup_menu, gtk.STOCK_DIRECTORY, "Open Folder", self.on_open_folder)
		self.popup_menu.add(gtk.SeparatorMenuItem())
		self.add_menuitem   (self.popup_menu, gtk.STOCK_GO_UP,                    self.on_move_up)
		self.add_menuitem   (self.popup_menu, gtk.STOCK_GO_DOWN,                  self.on_move_down)
		self.popup_menu.add(gtk.SeparatorMenuItem())
		self.add_menuitem   (self.popup_menu, gtk.STOCK_REMOVE,                   self.on_remove)
		self.popup_menu.show_all()

		# Finalize main window

		self.window.add(vbox)
		self.position_window()
		self.window.show_all()

	def add_altstock_button(self, toolbar, stock_id, alt_text, callback, tooltip):
		icon = gtk.Image()
		icon.set_from_stock(stock_id, gtk.ICON_SIZE_LARGE_TOOLBAR)

		btn = gtk.ToolButton()
		btn.set_icon_widget(icon)
		btn.set_label(alt_text)
		btn.connect("clicked", callback)
		try:
			btn.set_tooltip_text(tooltip)
		except AttributeError:
			pass # No tooltips, not the end of the world. Only python-gtk2 2.12 has them, i.e., Gutsy and above

		toolbar.insert(btn, -1)

	def add_stock_button(self, toolbar, stock_id, callback, tooltip):
		btn = gtk.ToolButton(stock_id)
		btn.connect("clicked", callback)
		try:
			btn.set_tooltip_text(tooltip)
		except AttributeError:
			pass # No tooltips, not the end of the world. Only python-gtk2 2.12 has them, i.e., Gutsy and above
		toolbar.insert(btn, -1)

	def add_menuitem(self, menu, stock_id, callback):
		item = gtk.ImageMenuItem(stock_id)
		item.connect("activate", callback)
		menu.append(item)

	def add_altmenuitem(self, menu, stock_id, alt_text, callback):
		icon = gtk.Image()
		icon.set_from_stock(stock_id, gtk.ICON_SIZE_LARGE_TOOLBAR)

		item = gtk.ImageMenuItem()
		item.set_image(icon)
		item.add(gtk.Label(alt_text)) # Weird, that GTK forces us to do this...
		item.connect("activate", callback)
		menu.append(item)

	def append_column(self, treeview, column, sort_id=None, foreground_id=None, strikethrough_id=None):
		renderer = column.get_cell_renderers()[0]
		if sort_id is not None:
			column.set_sort_column_id(sort_id)
		if foreground_id is not None:
			column.add_attribute(renderer, "foreground",    foreground_id)
		if strikethrough_id is not None:
			column.add_attribute(renderer, "strikethrough", strikethrough_id)
		treeview.append_column(column)
		return column

	def position_window(self):

		# This function was converted from a C function in clock.c in the GNOME
		# panel applet 'clock', which is GPL 2+ (like this code). Thanks to the
		# GNOME authors,
		#       Miguel de Icaza
 		#       Frederico Mena
		#       Stuart Parmenter
		#       Alexander Larsson
		#       George Lebl
		#       Gediminas Paulauskas
		#       Mark McLoughlin

		x,y = self.panel_image.window.get_origin()

		w,h = self.window.get_size()

		button_w = self.panel_image.get_allocation().width
		button_h = self.panel_image.get_allocation().height

		screen = self.window.get_screen()

		n = screen.get_n_monitors()
		found_monitor = False
		monitor = None
		for i in range(n):
			monitor = screen.get_monitor_geometry(i)
			if (x >= monitor.x and x <= monitor.x + monitor.width and
			    y >= monitor.y and y <= monitor.y + monitor.height):
				found_monitor = True
				break

		if not found_monitor:
			monitor = gtk.gdk.Rectangle()
			monitor.x = 0
			monitor.y = 0
			monitor.width  = screen.get_width()
			monitor.height = screen.get_height()

		if x > monitor.width/2:
			x -= w
			x += button_w

		if y < monitor.height/2:
			y += button_h
		else:
			y -= h

		self.window.move(x, y)


	# Threads

	def check_file(self, iter):
		filename = to_filename(self.files_store.get(iter, 0)[0])
		if os.path.exists(filename):
			self.files_store.set(iter, FILE_STORE_COLOR,         NORMAL_FG_COLOR)
			self.files_store.set(iter, FILE_STORE_STRIKETHROUGH, False)
		else:
			self.files_store.set(iter, FILE_STORE_COLOR,         DISABLE_FG_COLOR)
			self.files_store.set(iter, FILE_STORE_STRIKETHROUGH, True)

	def check_all_files(self):
		if self.pressed:
			iter = self.files_store.get_iter_first()
			while iter is not None:
				self.check_file(iter)
				iter = self.files_store.iter_next(iter)
			return True
		else:
			return False

	# Utilities

	def get_selected(self):
		model, paths = self.treeview.get_selection().get_selected_rows()
		iters = []
		for path in paths:
			iters.append(model.get_iter(path))
		return model, iters

	def get_n_files(self):
		ret = self.files_store.iter_n_children(None)
		return ret

	def swap_file_position(self, model, iter, old_val, new_val):
		def swap_file_internal(m, p, i, vs):
			if m.get(i, 1)[0] == vs[1]:
				m.set(i, 1, vs[0])
			              
		if old_val >= 0 and new_val >= 0 and old_val < self.get_n_files() and new_val < self.get_n_files():
			model.foreach(swap_file_internal, (old_val, new_val))
			model.set(iter, 1, new_val)

		self.pickle()

	def remove_file_position(self, model, old_position):
		def remove_file_internal(m, p, i, op):
			if m.get(i, 1)[0] > op:
				m.set(i, 1, m.get(i, 1)[0]-1)

		model.foreach(remove_file_internal, old_position)


### Factory

def factory(applet, iid, standalone_window=None):
	ts = TopShelf(applet, standalone_window)

	return True


### Main

if len(sys.argv) == 1:
	# Run standalone
	main_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
	main_window.set_title("TopShelf")
	main_window.connect("destroy", gtk.main_quit)

	applet = gnomeapplet.Applet()
	factory(applet, None, standalone_window=main_window)
	applet.reparent(main_window)
	main_window.show_all()
	gtk.main()

	sys.exit()

if __name__ == '__main__':
	gnomeapplet.bonobo_factory("OAFIID:Gnome_Panel_TopShelf_Factory", gnomeapplet.Applet.__gtype__, "A current files manager", "1.0", factory)

